import React from 'react';
import { SvgIcon, SvgIconProps } from '@material-ui/core';
import { ReactComponent as AchievementIcon } from './images/achievement.svg';

export default function Achievements(props: SvgIconProps) {
  return <SvgIcon component={AchievementIcon} viewBox="0 0 1024 1024" {...props} />;
}
